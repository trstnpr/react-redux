import * as actionTypes from '../actions/actionTypes';
import { updateObject } from './../utility';

const initialState = {
    results: []
}

const deleteResult = (state, action) => {
    const updatedArray = state.results.filter(result => result.id !== action.resultElId);
    return updateObject( state, { results: updatedArray } );
}

const resultReducer = (state = initialState, action) => {

    /*
    * Redux Immutable Update Patterns
    * https://redux.js.org/recipes/structuring-reducers/immutable-update-patterns
    */

    switch ( action.type ) {
        case actionTypes.STORE_RESULT:
            /*
            * .concat() - returns new array from old array plus the argument (CORRECT)
            * ,push() - manipulates the original value (INCORRECT)
            */
            return updateObject( state, { results: state.results.concat({id: new Date().getTime(), value: action.result}) } );
        case actionTypes.DELETE_RESULT:
            return deleteResult(state, action);
    }

    return state;
}

export default resultReducer;